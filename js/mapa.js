var map, marker = '', infowindow, geocoder,markerList = [], polylines = [], circle;
var directionsService, directionsDisplay;
var fatec = {lat:-23.19424138798818, lng: -45.890742411547876};
$(function(){
	map = new google.maps.Map(
		document.getElementById('mapa'), {
			center: fatec,
			zoom: 14,
			mapTypeId: 'roadmap'
		}
	);
	geocoder = new google.maps.Geocoder;
	//cria um marcador
	// marker = new google.maps.Marker({
	// 	map: map,
	// 	draggable:true,
	// 	anchorPoint: new google.maps.Point(0, -29),
	// 	icon: {
	// 		url: "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png",
	// 		size: new google.maps.Size(71, 71),
	// 		origin: new google.maps.Point(0, 0),
	// 		anchor: new google.maps.Point(17, 34),
	// 		scaledSize: new google.maps.Size(35, 35)
	// 	},
	// 	visible:true
	// });


/*
	marker.addListener('dragend',function(e){
		geocodeLatLng();
	});
*/
	infowindow = new google.maps.InfoWindow();
	//tenta obter a posição atual do cliente e posiciona o marcador no mapa
	// getClienteLocation();
	directionsService = new google.maps.DirectionsService;
	directionsDisplay = new google.maps.DirectionsRenderer({
		draggable: true
	});
	directionsDisplay.setMap(map);
});

function setRotaByMarkers() {
	var cordenada,waypoints = [],origem,destino;
	for (var i = 1; i < markerList.length-1; i++) {
		waypoints.push({location: markerList[i].position, stopover:true});
	}

		origem = markerList[0].position;

		destino = markerList[markerList.length-1].position;
		console.log(destino);
		console.log(origem);
		console.log(waypoints);
		markersVisibleFalse();
		calculateAndDisplayRoute(origem,destino,waypoints);
}

function markersVisibleFalse() {
	for (var i = 0; i < markerList.length; i++) {
		markerList[i].visible = false;
	}
}


/* usado para obter a localização do cliente */
function getClienteLocation(){
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position){
			var latlng = {lat:position.coords.latitude, lng:position.coords.longitude};
			geocoder.geocode({'location': latlng}, function(results, status){
				if( status === 'OK' ){
					if (results[0]){
						address = results[0].formatted_address;
						infowindow.setContent(address);
						infowindow.open(map, marker);
						map.setCenter(results[0].geometry.location);
						map.setZoom(15);
						marker.setPosition(results[0].geometry.location);
						marker.setVisible(true);
					}
				}
			});
		});
	}
}


/* invocado após o usuário arrastar o marcador no mapa */
function geocodeLatLng(){
	if( marker && marker.getPosition() ){
		var latlng = { lat:marker.getPosition().lat(), lng:marker.getPosition().lng() };
		geocoder.geocode({'location': latlng}, function(results, status){
			if( status === 'OK' ){
				if (results[0]) {
					address = results[0].formatted_address;
					infowindow.setContent(address);
					infowindow.open(map, marker);
				}
				else
				window.alert('Nenhum endereço localizado');
			}
			else{
				window.alert('Falha na geocodificação: ' + status);
			}
		});
	}
}

//https://developers.google.com/maps/documentation/javascript/directions?hl=pt-br
//https://developers.google.com/maps/documentation/javascript/examples/directions-simple?hl=pt-br
/*
function calculateAndDisplayRoute(a = []){
	for (var i = 1; i < a.length-1; i++) {
		directionsService.route(
			{
				origin: a[i-1],
				destination: a[i],
				travelMode: 'DRIVING'
			},
			function(response, status){
				if (status === 'OK'){
					directionsDisplay.setDirections(response);
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			}
		);
	}
}
*/



function calculateAndDisplayRoute(origem,destino,pontos) {
	return directionsService.route(
		{
			origin: origem,
			destination: destino,
			waypoints:pontos,
			travelMode: 'DRIVING'
		},
		//destination: "Rua Teopompo de Vasconcelos - Vila Adyana, São José dos Campos - SP, Brasil",
		//"FATEC São José dos Campos - Prof. Jessen Vidal - Avenida Cesare Mansueto Giulio Lattes - Eugênio de Melo, São José dos Campos - SP, Brasil"
		function(response, status){
			if (status === 'OK'){
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
		}
	);

}

//key da api de android -> AIzaSyApUAbLDWKrU3188tdZrSMRmbBCq8sMyb0
/* link para json por endereço:markerList.length
https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=
*/
function setCenter(latLng){
	map.setCenter(latLng);
}

function getlatlgn() {
	var str = "",str2 = "",str_return = "";
	for (var i = 0; i < markerList.length; i++) {
		str += markerList[i].getPosition()+"|";
	}
	str = str.split("|");
	for (var i = 0; i < str.length-1; i++) {
		str2 = str[i].slice(1,-1).split(",");
		str_return += str2[0].trim()+" "+str2[1].trim()+"|";
	}
	return str_return;
}

function getRota() {
	$.ajax({
		type: 'get',
		url:'prog/linha.php',
		data:{op:'view'},
		success: function(response){
			var json = JSON.parse(response);

		}
	});
}

function cleanCircle() {
	if (circle != null) {
		circle.setVisible(false);
	}
}

function setCircle(latLng) {
	circle = new google.maps.Circle({
		strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
		visible:true,
		map:map,
		center:latLng,
		radius:1000
	});
	console.log(circle);
}

function setRota(json) {
	var pontos,latlngv, cordenada,waypoints = [],origem,destino;
	pontos = json['pontos'].split('|');
	for (var i = 1; i < pontos.length-2; i++) {
		latlgnv = pontos[i].split(' ');
		console.log(Number(latlgnv[1]));
		cordenada = {lat: Number(latlgnv[0]), lng: Number(latlgnv[1])};
		waypoints.push({location:cordenada,stopover:true});
	}

		latlgnv = pontos[0].split(' ');
		origem = {lat: Number(latlgnv[0]), lng: Number(latlgnv[1])};

		latlgnv = pontos[pontos.length-2].split(' ');
		destino = {lat: Number(latlgnv[0]), lng: Number(latlgnv[1])};
		console.log(destino);
		calculateAndDisplayRoute(origem,destino,waypoints);
}

function decodeRoute(route) {
	route.routes[0].legs.forEach(function(leg){
		leg.steps.forEach(function(step){
			step.path = google.maps.geometry.encoding.decodePath(step.polyline.points);
		});
	});

	route.routes[0].overview_path = google.maps.geometry.encoding.decodePath(route.routes[0].overview_polyline);

	return route;
}
