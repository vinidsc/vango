var rota_teste;
var colors = ["#DBA901", "#01DF01", "#01A9DB", "#7401DF", "#848484"];
$(function(){
  console.log('ok');
  $("#titulo").click(function(e){
    console.log(markerList);
  });
	$("#menu-logar").click(function(e){
		e.preventDefault();
    console.log("Ok");
		$("#mdLogin").modal();
	});
	$("#menu-sair").click(function(e){
		e.preventDefault();
		login.logout();
	});
	$('#mdLogin-btnLogar').click(function(e){
		e.preventDefault();
		login.log();
		console.log(idProprietario);
	});
	$('#menu-cadastrar').click(function(e){
		e.preventDefault();
		$("#mdCadastro").modal();
	});
	$('#mdCadastro-btnSubmeter').click(function(e){
		e.preventDefault();
    console.log(e);
		login.cadastrar();
	});
	$('#menu-editar').click(function(e){
		e.preventDefault();
		if( idProprietario != '' ){
			$("#mdEditarCadastro-mail").val(mail);
			$("#mdEditarCadastro-fone").val(fone);
			$('#mdEditarCadastro-fone').trigger('blur');
			$("#mdEditarCadastro").modal();
		}
	});

  $("#linha-cadastrar").click(function(e){
    e.preventDefault();
    linhaSplit = getlatlgn().split("|");
    $("#modalLinha").modal();
  });
  $('#submit-cadastro-linha').click(function(e){
    e.preventDefault();
    submitLinha();
    console.log("submit");
  });
  $('#cancelar-cadastro-linha').click(function(e){
    e.preventDefault();
    console.log("cancelar");
  });

	$('#mdEditarCadastro-btnSubmeter').click(function(e){
		e.preventDefault();
		login.atualizar();
	});

	$('#menu-editarsenha').click(function(e){
		e.preventDefault();
		$('#mdEditarSenha .has-error').removeClass('has-error');
		$('#mdEditarSenha-alert .close').trigger('click');
		$('#mdEditarSenha-senha,#mdEditarSenha-confirma-senha').val('');
		$("#mdEditarSenha").modal();
	});
	$('#mdEditarSenha-btnSubmeter').click(function(e){
		e.preventDefault();
		login.editarSenha();
	});

	$('.alert>.close').click(function(e){
		e.preventDefault();
		$(this).parent('.alert').hide();
	});

  [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
    // in case the input is already filled..
    if( inputEl.value.trim() !== '' ) {
      classie.add( inputEl.parentNode, 'input--filled' );
    }

    // events:
    inputEl.addEventListener( 'focus', onInputFocus );
    inputEl.addEventListener( 'blur', onInputBlur );
  } );

  function onInputFocus( ev ) {
    classie.add( ev.target.parentNode, 'input--filled' );
  }

  function onInputBlur( ev ) {
    if( ev.target.value.trim() === '' ) {
      classie.remove( ev.target.parentNode, 'input--filled' );
    }
  }

	$("#mdLogin-password").keypress(function(e){
		if( e.which == 13 ) $('#mdLogin-btnLogar').trigger('click');
	});
	$("#mdEditarSenha-confirma-senha").keypress(function(e){
		if( e.which == 13 ) $('#mdEditarSenha-btnSubmeter').trigger('click');
	});
	$("#mdCadastro-confirma-senha").keypress(function(e){
		if( e.which == 13 ) $('#mdCadastro-btnSubmeter').trigger('click');
	});
	$("#mdEditarCadastro-fone").keypress(function(e){
		if( e.which == 13 ) $('#mdEditarCadastro-btnSubmeter').trigger('click');
	});
  document.querySelectorAll(".linha-element-list").forEach(function(e){
    let url = 'src/vango_ws.php/linha/';
    console.log(e.id);
    e.addEventListener('click',function(e){
      console.log(e.target.id);
      $.ajax({
        type:'get',
        url:'src/vango_ws.php/linha',
        data:{nome:e.target.id},
        timeout:1000,
        success:function(e){
          // console.log(e);
          console.log(JSON.parse(JSON.parse(e)[0].rota));
          setCenter(decodeRoute(JSON.parse(JSON.parse(e)[0].rota)).routes[0].overview_path[0]);
          directionsDisplay.setDirections(decodeRoute(JSON.parse(JSON.parse(e)[0].rota)));
          console.log('success');
        },
        error:function(e){
          console.log(e);
          console.log('erro');
        }
      });
    });
  });
  // console.log($('.linha-element-list'));
  // document.querySelectorAll('.linha-element-list').forEach(function(){
  //   console.log('ok');
  // });

	/* mask fone */
	$('#mdCadastro-fone,#mdEditarCadastro-fone')
	.keydown(function (e) {
		var key = e.which || e.charCode || e.keyCode || 0;
		if( key == 8 || key == 9 || key == 46 || //backspace, tab, delete
				(key >= 37 && key <= 40) ) //setas
			return true;

		campo = $(this);
		//inicia com o (
		if(campo.val().length === 1 && (key === 8 || key === 46)) {
			campo.val('(');
			return false;
		}
		// Reset if they highlight and type over first char.
		else if (campo.val().charAt(0) !== '(') {
			campo.val('('+String.fromCharCode(e.keyCode)+'');
		}

		if( !(key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40)) ) {
			if (campo.val().length === 3)
				campo.val(campo.val() + ')');
			if (campo.val().length === 4)
				campo.val(campo.val() + ' ');
			if (campo.val().length === 10)
				campo.val(campo.val() + '-');
		}

		return (key >= 48 && key <= 57); //dígitos
	})
	.bind('focus click', function () {
		campo = $(this);
		if (campo.val().length === 0) {
			campo.val('(');
		}
		else {
			var val = campo.val();
			campo.val('').val(val); // Ensure cursor remains at the end
		}
	})
	.blur(function () {
		$(this).val( formatCelular( $(this).val() ) );
	});
});

function submitLinha() {
  var linhaSplit = getlatlgn().split("|");
  var idmodelo,
      modelo = $("#modeloVan").val().trim(),
      horario = $("#horario").val().trim(),
      nrovagas = $("#nrovagas").val().trim(),
      ano = $("#ano").val().trim(),
      nome = $("#nome").val().trim(),
      origem = linhaSplit[0],
      destino = linhaSplit[linhaSplit.length-2];
      // rota = directionsDisplay.getDirections();
      console.log(directionsDisplay.getDirections());
    $.ajax({
      type:'post',
      url:'src/vango_ws.php/cadastrar-linha',
      data:{idmodelo:1,pontos:getlatlgn(),origem:origem,destino:destino,horario:horario,nrovaga:nrovagas,ano:ano,acessibilidade:1,nome:nome,rota:JSON.stringify(directionsDisplay.getDirections())},
      timeout:10000,
      success:function(e){
        console.log(e);
        console.log("success");
        location.reload();
      },
      error:function(e){
        console.log(e);
        console.log("error");
      }
    });
    console.log('aaaaaaaa');
    // $.post('src/vango_ws.php/cadastrar-linha',{idmodelo:1,pontos:getlatlgn(),origem:origem,destino:destino,horario:horario,nrovaga:nrovagas,ano:ano,acessibilidade:1,nome:nome,rota:JSON.stringify(directionsDisplay.getDirections())},function(data,status,xhr){
    //   console.log(data);
    //   console.log(status);
    //   console.log(xhr);
    // });
}

function testeSubmit() {
  $.ajax({
    type:'get',
    url:'src/vango_ws.php/teste',
    timeout:10000,
    success:function(e){
      console.log(e);
    },
    error:function(e){
      console.log(e);
    }
  });
}

function formatCelular(nro){
	nro = nro.replace(/\D/g,'');
	if( nro.length == '' )
		return '';
	var r = '(';
	for( var i = 0; i < nro.length && i < 11; i++ ){
		if( i == 2 )
			r += ') ';
		else if( i == 7 )
			r += '-';
		r += nro.charAt(i);
	}
	return r;
}

var login = {
	request:false,
	log:function(){
		var email = $('#mdLogin-mail').val().trim().toLowerCase(),
			senha = $('#mdLogin-password').val().trim(),
			emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		$('#mdLogin .has-error').removeClass('has-error');
		$('#mdLogin-alert .close').trigger('click');
		if( email == "" ){
			$("#mdLogin-mail").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdLogin-alert'),'alert-danger', "Por favor, forneça o seu e-mail de login.");
		}
		else if( !emailRegex.test(email) ){
			$("#mdLogin-password").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdLogin-alert'),'alert-danger', "O email parece mal formado, certifique-se que ele esteja correto.");
		}
		else if( senha.length < 6 || senha.length > 10 ){
			$("#mdLogin-password").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdLogin-alert'),'alert-danger', "A senha precisa ter de 6 a 10 caracteres.");
		}
		else if( !login.request ){
			$.ajax({
				type: 'get',
				url: 'src/vango_ws.php/login',
				data: {mail:email,senha:senha},
				timeout:10000,
				beforeSend:function(x){
					login.request = true;
					$('.mdLogin-btn').button('loading');
				},
				complete:function(a,b){
					login.request = false;
					$('.mdLogin-btn').button('reset');
				},
				error:function(a,status,c){
					login.request = false;
					$('.mdLogin-btn').button('reset');
				},
				success:function( resp ){
					try{
						var r = eval('(' + resp + ')');
						if( r.erro != '' )
							setMsgAlert($('#mdLogin-alert'),'alert-danger',r.erro);
						else{
              location.reload();
						}
					}catch(e){
						setMsgAlert($('#mdLogin-alert'),'alert-danger',"Problemas ao completar a operação.");
					}
					login.request = false;
				}
			});
		}
		else
			setMsgAlert($('#mdLogin-alert'),'alert-danger',"Existe uma solicitação pendente, por favor, aguarde...");
	},
	logout:function(){
		if( !login.request ){
			$.ajax({
				type: 'get',
				url: 'src/vango_ws.php/logout',
				timeout:10000,
				beforeSend:function(x){
					login.request = true;
					waitingDialog.show('Encerrando o acesso',{dialogSize:'sm',rtl:false});
    			},
				complete:function(a,b){
					login.request = false;
					waitingDialog.hide();
				},
				error:function(a,status,c){
					login.request = false;
					waitingDialog.hide();
				},
				success:function( resp ){
					location.reload();
				}
			});
		}
	},
	cadastrar:function(){
		var mail = $('#mdCadastro-mail').val().trim().toLowerCase(),
			fone = $('#mdCadastro-fone').val().trim().replace(/\D/g,''),
			senha = $('#mdCadastro-senha').val().trim(),
			confirmacao = $('#mdCadastro-confirma-senha').val().trim(),
			emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		$('.has-error').removeClass('has-error');
		// $('#mdCadastro-alert .close').trigger('click');
		if( mail == '' ){
			$("#mdCadastro-mail").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdCadastro-alert'),'alert-danger',"Forneça o seu e-mail, pois é uma forma de contato.");
		}
		else if( !emailRegex.test(mail) ){
			$("#mdCadastro-mail").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdCadastro-alert'),'alert-danger', "O e-mail parece mal formado, certifique-se que ele esteja correto.");
		}
		else if( fone.length != 11 ){
			$("#mdCadastro-fone").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdCadastro-alert'),'alert-danger',"O número do fone deverá estar no formato (dd) ddddd-dddd.");
		}
		else if( senha.length < 6 || senha.length > 10 ){
			$("#mdCadastro-senha").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdCadastro-alert'),'alert-danger',"A senha precisa ter de 6 a 10 caracteres.");
		}
		else if( senha != confirmacao ){
			$("#mdCadastro-confirma-senha").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdCadastro-alert'),'alert-danger',"A confirmação da senha precisa ser igual a senha.");
		}
		else if( !login.request ){
			$.ajax({
				type: 'post',
				url: 'src/vango_ws.php/cadastro',
				data: {mail:mail,fone:fone,senha:senha},
				timeout:10000,
				beforeSend:function(x){
					login.request = true;
					$('.modalCadastro-btn').button('loading');
				},
				complete:function(a,b){
					login.request = false;
					$('.modalCadastro-btn').button('reset');
				},
				error:function(a,status,c){
          console.log(a);
					login.request = false;
					$('.modalCadastro-btn').button('reset');
				},
				success:function( resp ){
          console.log(r);
					try{
						// var r = eval('(' + resp + ')');
            var r = JSON.parse(resp);
						if( r.erro != "" )
							setMsgAlert($('#mdCadastro-alert'),'alert-danger',r.erro);
						else{
							// setMsgAlert($('#mdCadastro-alert'),'alert-success', 'Cadastro efetivado com sucesso. Você já está logado.');
							$('#mdCadastro-mail,#mdCadastro-fone,#mdCadastro-senha,#mdCadastro-confirma-senha').val('');
              location.reload();
						}
					}catch(e){
						setMsgAlert($('#mdCadastro-alert'),'alert-danger',"Problemas ao completar a operação.");
					}
					login.request = false;
				}
			});
		}
		else
			setMsgAlert($('#mdCadastro-alert'),'alert-danger',"Existe uma solicitação pendente, por favor, aguarde...");
	},
	atualizar:function(){
		var mail = $('#mdEditarCadastro-mail').val().trim().toLowerCase(),
			fone = $('#mdEditarCadastro-fone').val().trim().replace(/\D/g,''),
			emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		$('.has-error').removeClass('has-error');
		$('#mdEditarCadastro-alert .close').trigger('click');
		if( mail == '' ){
			$("#mdEditarCadastro-mail").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger',"Forneça o seu e-mail, pois é uma forma de contato.");
		}
		else if( !emailRegex.test(mail) ){
			$("#mdEditarCadastro-mail").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger', "O e-mail parece mal formado, certifique-se que ele esteja correto.");
		}
		else if( fone.length != 11 ){
			$("#mdEditarCadastro-fone").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger',"O número do fone deverá estar no formato (dd) ddddd-dddd.");
		}
		else if( !login.request ){
			$.ajax({
				type: 'put',
				url: 'src/vango_ws.php/cadastro',
				data: {mail:mail,fone:fone},
				timeout:10000,
				beforeSend:function(x){
					login.request = true;
					$('.mdEditarCadastro-btn').button('loading');
				},
				complete:function(a,b){
					login.request = false;
					$('.mdEditarCadastro-btn').button('reset');
				},
				error:function(a,status,c){
					login.request = false;
					$('.mdEditarCadastro-btn').button('reset');
				},
				success:function( r ){
					try{
						// var r = eval('(' + resp + ')');
						if( r.erro != '' )
							setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger',r.erro);
						else{
							setMsgAlert($('#mdEditarCadastro-alert'),'alert-success', 'Cadastro atualizado com sucesso.');
							location.reload();
						}
					}catch(e){
            console.log(e);
						setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger',"Problemas ao completar a operação.");
					}
					login.request = false;
				}
			});
		}
		else
			setMsgAlert($('#mdEditarCadastro-alert'),'alert-danger',"Existe uma solicitação pendente, por favor, aguarde...");
	},
	editarSenha:function(){
		var senha = $('#mdEditarSenha-senha').val().trim(),
			confirmacao = $('#mdEditarSenha-confirma-senha').val().trim(),
			emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		$('.has-error').removeClass('has-error');
		$('#mdEditarSenha-alert .close').trigger('click');
		if( senha.length < 6 || senha.length > 10 ){
			$("#mdEditarSenha-senha").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdEditarSenha-alert'),'alert-danger',"A senha precisa ter de 6 a 10 caracteres.");
		}
		else if( senha != confirmacao ){
			$("#mdEditarSenha-confirma-senha").parent('.input-group').addClass('has-error');
			setMsgAlert($('#mdEditarSenha-alert'),'alert-danger',"A confirmação da senha precisa ser igual a senha.");
		}
		else if( !login.request ){
			$.ajax({
				type: 'put',
				url: 'src/vango_ws.php/senha',
				data: {senha:senha},
				timeout:10000,
				beforeSend:function(x){
					login.request = true;
					$('.mdEditarSenha-btn').button('loading');
				},
				complete:function(a,b){
					login.request = false;
					$('.mdEditarSenha-btn').button('reset');
				},
				error:function(a,status,c){
					login.request = false;
					$('.mdEditarSenha-btn').button('reset');
				},
				success:function( r ){
					try{
						// var r = eval('(' + resp + ')');
						if( r.erro != '' )
							setMsgAlert($('#mdEditarSenha-alert'),'alert-danger',r.erro);
						else{
							setMsgAlert($('#mdEditarSenha-alert'),'alert-success', 'Senha alterada com sucesso.');
							$('#mdEditarSenha-senha,#mdEditarSenha-confirma-senha').val('');
              location.reload();
						}
					}catch(e){
						setMsgAlert($('#mdEditarSenha-alert'),'alert-danger',"Problemas ao completar a operação.");
					}
					login.request = false;
				}
			});
		}
		else
			setMsgAlert($('#mdEditarSenha-alert'),'alert-danger',"Existe uma solicitação pendente, por favor, aguarde...");
	}
}

function setPolylineRotas(e) {
  $.ajax({
    type:'get',
    url:'src/vango_ws.php/range',
    data:{lat:e.latLng.lat(),lng:e.latLng.lng()},
    timeout:10000,
    success:function(e){
      // console.log(e);
      rota = JSON.parse(e);
      for (var i = 0; i < rota.length; i++) {
        // console.log(JSON.parse(rota[i]));
        // directionsDisplay.setDirections(decodeRoute(rota[i]))
        polylines.push(new google.maps.Polyline({
          map:map,
          path:JSON.parse(rota[i]).routes[0].overview_path,
          strokeColor: colors[i],
          visible:true
        }));
      }
    },
    error:function(e){
      console.log('erro');
      console.log(e);
    }
  });
}

function cleanPolylineRotas() {
  for (var i = 0; i < polylines.length; i++) {
    polylines[i].setVisible(false);
  }
  polylines = [];
}

function setMsgAlert(obj,classe,corpo){
	$(obj).removeClass('alert-danger');
	$(obj).removeClass('alert-warning');
	$(obj).removeClass('alert-success');
	$(obj).addClass(classe);
	$(obj).children('span').html(corpo);
	$(obj).show();
}
