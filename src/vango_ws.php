<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require_once 'Proprietario.php';
require_once 'Linha.php';

$app = new \Slim\App;

$app->get('/login', function (Request $request, Response $response, array $args) {

    $queryParams = $request->getQueryParams();

    $proprietario = new Proprietario();
    $linhas = new Linha();

    $proprietarioData = $proprietario->login($queryParams['mail'],$queryParams['senha']);

    if ($proprietarioData == false) {
      $response->getBody()->write(json_encode(array("login" => "false")));
    }else{
      session_start();
      $_SESSION['mail'] = $proprietarioData[0]['mail'];
      $_SESSION['fone'] = $proprietarioData[0]['fone'];
      $_SESSION['id'] = $proprietarioData[0]['id'];
      $_SESSION['senha'] = $proprietarioData[0]['senha'];
      $_SESSION['login'] = true;
      $_SESSION['linhas'] = $linhas->getByProprietarioId($proprietarioData[0]['id']);
      $_SESSION['erro'] = '';
      $response->getBody()->write(json_encode($_SESSION));
    }
    return $response;
});

$app->get('/logout', function (Request $request, Response $response, array $args) {

    session_start();
    foreach ($_SESSION as $key => $session) {
      $_SESSION[$key] = '';
    }

    $response->getBody()->write(json_encode($_SESSION));

    return $response;
});

$app->post('/cadastro', function (Request $request, Response $response, array $args) {

    $queryParams = $request->getParsedBody();

    $proprietario = new Proprietario();

    $data = $proprietario->insert($queryParams['mail'], $queryParams['fone'], $queryParams['senha']);

    session_start();
    $_SESSION['mail'] = $data['mail'];
    $_SESSION['fone'] = $data['fone'];
    $_SESSION['id'] = $data['id'];
    $_SESSION['senha'] = $data['senha'];
    $_SESSION['login'] = true;
    $_SESSION['linhas'] = '';
    $_SESSION['erro'] = '';

    $response->getBody()->write(json_encode($data));

    return $response;
});


$app->get('/teste', function (Request $request, Response $response, array $args) {

    session_start();
    $response->getBody()->write(json_encode($_SESSION));

    return $response;
});

$app->post('/cadastrar-linha', function(Request $request, Response $response, array $args){
  session_start();
  $queryParams = $request->getParsedBody();

  $linha = new Linha();

  $data = $linha->insert($queryParams['idmodelo'], $_SESSION['id'], $queryParams['pontos'],
    json_decode($queryParams['rota'])->routes[0]->overview_path,$queryParams['origem'],$queryParams['destino'],
    $queryParams['horario'],$queryParams['nrovaga'],$queryParams['ano'],$queryParams['acessibilidade'],
    $queryParams['nome'],base64_encode($queryParams['rota']));

  $_SESSION['linhas'] = $linha->getByProprietarioId($_SESSION['id']);

  $response->getBody()->write(json_encode($data));

  return $response;
});

$app->get('/linha', function (Request $request, Response $response, array $args) {

    session_start();

    $linha = new Linha();

    $queryParams = $request->getQueryParams();

    $data = $linha->getByNameAndIdproprietario($queryParams['nome'], $_SESSION['id']);

    $data[0]['rota'] = base64_decode($data[0]['rota']);

    $response->getBody()->write(json_encode($data));

    return $response;
});

$app->get('/range', function(Request $request, Response $response, array $args){
  $linha = new Linha();

  $queryParams = $request->getQueryParams();

  $result = $linha->getRotaByRange($queryParams['lat'], $queryParams['lng']);

  $rota = [];
  foreach ($result as $key => $route) {
    $route = base64_decode($route['rota']);
    array_push($rota, $route);
  }

  $response->getBody()->write(json_encode($rota));

});

$app->put('/cadastro',function(Request $request, Response $response, array $args){

  session_start();

  $proprietario = new Proprietario();

  $queryParams = $request->getQueryParams();

  $data = $proprietario->update_cadastro($_SESSION['id'], $queryParams['mail'], $queryParams['fone']);

  $response->getBody()->write(json_encode($data));
});

$app->put('/senha',function(Request $request, Response $response, array $args){

  session_start();

  $proprietario = new Proprietario();

  $queryParams = $request->getQueryParams();

  $data = $proprietario->update_senha($_SESSION['id'], $queryParams['senha']);

  $response->getBody()->write(json_encode($data));
});

$app->run();
