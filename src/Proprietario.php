<?php

require_once 'Conexao.php';

class Proprietario{

  private $conexao;

  function __construct(){
    $this->conexao = new Conexao();
  }

  public function insert($mail,$fone,$senha){
    $con = $this->conexao->connect();
    $sql = "INSERT INTO tbproprietario (mail,fone,senha) VALUES ('$mail','$fone','$senha') RETURNING *";
    $result = pg_fetch_all(pg_query($con,$sql));
    pg_close($con);
    return $result[0] + array("erro" => '');
    // return $result[0] +array("mail" => $mail, "fone" => $fone, "senha" => $senha, "erro" => "");
  }

  public function update($id,$mail,$fone,$senha){
    $con = $this->conexao->connect();
    $sql = "UPDATE tbproprietario set mail='$mail', fone='$fone', senha='$senha' where id=$id";
    pg_query($con,$sql);
    pg_close($con);
    return array("id" => $id, "mail" => $mail, "fone" => $fone, "senha" => $senha);
  }

  public function delete($id){
    $con = $this->conexao->connect();
    $sql = "DELETE FROM tbproprietario WHERE id=$id";
    pg_query($con,$sql);
    pg_close($con);
    return json_encode(
      array(
        "status" => "ok",
        "id" => $id
      )
    );
  }

  public function listAll(){
    $con = $this->conexao->connect();
    $sql = "SELECT * FROM tbproprietario";
    $result = pg_fetch_all(pg_query($con,$sql));
    pg_close($con);
    return json_encode($result);
  }

  public function getById($id){
    $con = $this->conexao->connect();
    $sql = "SELECT * FROM tbproprietario WHERE id=$id";
    $result = pg_fetch_all(pg_query($con,$sql));
    pg_close($con);
    return json_encode($result);
  }

  public function login($mail, $senha){
    $con = $this->conexao->connect();
    $sql = "SELECT * FROM tbproprietario WHERE mail='$mail' and senha='$senha'";
    $result = pg_fetch_all(pg_query($con,$sql));
    pg_close($con);
    return $result + array("erro" => '');
  }

  public function update_cadastro($id, $mail, $fone){
    $con = $this->conexao->connect();
    $sql = "UPDATE tbproprietario set mail='$mail', fone='$fone' where id=$id";
    pg_query($con,$sql);
    pg_close($con);
    return array("id" => $id, "mail" => $mail, "fone" => $fone, "erro" => '');
  }

  public function update_senha($id, $senha){
    $con = $this->conexao->connect();
    $sql = "UPDATE tbproprietario set senha='$senha' where id=$id";
    pg_query($con,$sql);
    pg_close($con);
    return array("id" => $id, "senha" => $senha, "erro" => '');
  }
}

 ?>
