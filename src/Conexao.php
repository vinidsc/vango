<?php

class Conexao{

	//parâmetros usados para acessar o SGBD da máquina localhost
	private $host = "localhost";
	private $user = "postgres";
	private $password = "";
	private  $bd = "vangodb";

	public function __construct(){
	}

  public function connect(){
    return pg_connect("host=$this->host dbname=$this->bd user=$this->user password=$this->password");
  }

}
?>
