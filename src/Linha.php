<?php

  require_once 'Conexao.php';

  class Linha
  {

    private $conexao;

    function __construct()
    {
      $this->conexao = new Conexao();
    }

    public function insert($idmodelo,$idproprietario,$pontos,$geom,$origem,$destino,$horario,$nrovaga,$ano,$acessibilidade,$nome,$rota){
      $path = $this->overview_pathToGeom($geom);
      // $path = 'asdas'
      $con = $this->conexao->connect();
      $sql = "INSERT INTO tblinha (idmodelo,idproprietario,pontos,geom,origem,destino,horario,nrovaga,ano,acessibilidade,nome,rota) VALUES ('$idmodelo','$idproprietario','$pontos',$path,'$origem','$destino','$horario','$nrovaga','$ano','$acessibilidade','$nome','$rota') RETURNING id";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return $result;
    }

    public function update($id,$idmodelo,$idproprietario,$pontos,$origem,$destino,$horario,$nrovaga,$ano,$acessibilidade,$nome){
      $con = $this->conexao->connect();
      $sql = "UPDATE tblinha set idmodelo='$idmodelo', idproprietario='$idproprietario', pontos='$pontos',
        origem='$origem', destino='$destino', horario='$horario', nrovaga='$nrovaga', ano='$ano',
        acessibilidade='$acessibilidade', nome='$nome', where id='$id'";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return json_encode(
        array(
          "id" => $result[0]['id'],
          "idmodelo" => $idmodelo,
          "idproprietario" => $idproprietario,
          "pontos" => $pontos,
          "origem" => $origem,
          "destino" => $destino,
          "horario" => $horario,
          "nrovaga" => $nrovaga,
          "ano" => $ano,
          "acessibilidade" => $acessibilidade,
          "nome" => $nome
        )
      );
    }

    public function delete($id){
      $con = $this->conexao->connect();
      echo $sql = "DELETE FROM tblinha WHERE id=$id";
      pg_query($con,$sql);
      pg_close($con);
      return json_encode(
        array(
          "status" => "ok",
          "id" => $id
        )
      );
    }

    public function listAll(){
      $con = $this->conexao->connect();
      $sql = "SELECT * FROM tblinha";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return json_encode($result);
    }

    public function getById($id){
      $con = $this->conexao->connect();
      $sql = "SELECT * FROM tblinha WHERE id='$id'";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return $result;
    }

    public function getByNameAndIdproprietario($name,$id)
    {
      $con = $this->conexao->connect();
      $sql = "SELECT * FROM tblinha WHERE nome='$name' AND idproprietario='$id'";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return $result;
    }

    public function getByProprietarioId($proprietarioId)
    {
      $con = $this->conexao->connect();
      $sql = "SELECT * FROM tblinha WHERE idproprietario=$proprietarioId";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return $result;
    }

    public function overview_pathToGeom($overview_path)
    {
      $geom = "ST_GeomFromText('LineString(";
      foreach ($overview_path as $key => $line) {
        $geom .= $line->lat.' '.$line->lng.',';
      }
      $geom = substr($geom, 0, -1).")', 4326)";
      return $geom;
    }

    public function getRotaByRange($lat, $lng)
    {
      $con = $this->conexao->connect();
      $sql = "select rota from tblinha where ST_Distance(geom, ST_GeomFromText('Point($lat $lng)', 4326)) < 0.01";
      $result = pg_fetch_all(pg_query($con,$sql));
      pg_close($con);
      return $result;
    }

  }
// geometry(LineString,4326)

  // $linha = new Linha();
  // $a = $linha->getRotaByRange("-23.18788982679138", "-45.88838178878359");
  // $rota = [];
  // foreach ($a as $key => $route) {
  //   $route = base64_decode($route['rota']);
  //   array_push($rota, $route);
  // }
  // var_dump($a);
  // var_dump($rota);
  // echo json_encode($rota);

?>
