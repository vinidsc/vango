<?php
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>AppVan</title>
		<meta charset="utf-8">
		<link rel="shortcut icon" type="image/png" sizes="32x32" href="imagens/favicon-16x16.png">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="js/mapa.js?t=<?php echo time();?>"></script>
		<script src="lib/textInputEffect/classie.js"></script>
		<script src="js/app.js" charset="utf-8"></script>
		<script src="lib/bootstrap/bootstrap-waitingfor.min.js"></script>
		<script src='https://maps.googleapis.com/maps/api/js?amp;language=pt_BR&amp;key=AIzaSyBRDCFxkCY5dwveBSOPUS6hOXq4aeqon4g&libraries=places' type='text/javascript'></script>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid">

        <!-- LOGO -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#r">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div id='titulo' class="navbar-brand">Van Go</div>
				</div>

        <!-- PROCURAR ROTAS -->
        <div class="navbar-form navbar-left search-place">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search" name="search">
          </div>
          <button class="btn btn-default">Submit</button>
        </div>

        <!-- DROPDOWN DO LOGIN -->
				<div class="" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<?php if((isset($_SESSION['login'])? $_SESSION['login']:"") == false): ?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span id='iconMenuLogin' class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;<span id='rotuloMenuLogin'>login</span>&nbsp;<span class="caret"></span></a>
							<?php else: ?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"></span>&nbsp;&nbsp;<span id='rotuloMenuLogin'><?php echo $_SESSION['mail'] ?></span>&nbsp;<span class="caret"></a>
							<?php endif; ?>
							<ul class="dropdown-menu" role="menu">
                <?php if((isset($_SESSION['login'])? $_SESSION['login']:"") == false): ?>
  								<li id='menu-logar'><a href="#"><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;Efetuar login</span></a></li>
  								<li id='menu-cadastrar'><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Cadastrar na plataforma</span></a></li>
                <?php else: ?>
  								<li id='menu-editarsenha'><a href="#"><span class="glyphicon glyphicon-lock"></span>&nbsp;&nbsp;Alterar sua senha</span></a></li>
  								<li id='menu-editar'><a href="#"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Alterar seus dados</a></li>
  								<li class="divider"></li>
  								<li id='menu-sair' class=""><a href="#"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Sair</a></li>
                <?php endif; ?>
							</ul>
						</li>
					</ul>
				</div>


        <?php if((isset($_SESSION['login'])? $_SESSION['login']:"") == true): ?>
          <!-- DROPDOWN DAS ROTAS -->
  				<div class="" id="linhaNavbar">
  					<ul class="nav navbar-nav navbar-right">
  						<li class="dropdown">
  							<a href="#" id="linha-dropdown" class="dropdown-toggle" data-toggle="dropdown"></span>&nbsp;&nbsp;<span>linhas</span>&nbsp;<span class="caret"></span></a>
  							<ul id="nome-linha-list" class="dropdown-menu" role="menu">
  								<li id='linha-cadastrar'><a href="#"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Cadastrar Rota</a></li>
  								<li class="divider"></li>
  								<?php foreach ($_SESSION['linhas'] as $linha): ?>
                    <li class="linha-element-list"><a id="<?php echo $linha['nome'] ?>" href="#"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;<?php echo $linha['nome'] ?></a></li>
                  <?php endforeach; ?>
  							</ul>
  						</li>
  					</ul>
  				</div>
        <?php endif; ?>

			</div>
		</nav>

    <!-- MAPA -->
		<div class="container-fluid" id="mapa" style='width:100%;height:100%;box-sizing: border-box;margin:0px !important'></div>

    <!-- JANELAS MODAIS -->
    <?php include_once("janelas.php"); ?>

		<?php if((isset($_SESSION['login'])? $_SESSION['login']:"") == true): ?>
			<script type="text/javascript">
				$(function(){
					map.addListener('click',function(e){
						markerList.push(new google.maps.Marker({
							map:map,
							position:e.latLng,
							visible:true
						}));
						if (markerList.length > 1) {
							setRotaByMarkers();
						}
					});
				});
			</script>
		<?php else: ?>
			<script type="text/javascript">
				$(function(){
					map.addListener('click',function(e){
						if (marker != '') {
							marker.setVisible(false);
						}
						marker = new google.maps.Marker({
							map:map,
							position:e.latLng,
							visible:true
						});
						cleanPolylineRotas();
						cleanCircle();
						setCircle(e.latLng);
						setPolylineRotas(e);
					});
				});
			</script>
		<?php endif; ?>

	</body>
</html>
