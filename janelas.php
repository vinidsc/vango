<!-- Modal login -->
<div class='modal fade' id='mdLogin' role='dialog' data-backdrop='static'>
	<div class='modal-dialog'>
		<!-- Modal content-->
		<div class='modal-content'>
			<div class='modal-header' style='padding:20px 40px;'>
				<!--<button type='button' class='close' data-dismiss='modal'>&times;</button>-->
				<h4>Login na plataforma</h4>
			</div>
			<div class='modal-body' style='padding:25px'>
				<form role='form' id='form-login' >
					<!-- <div style='margin-bottom: 15px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-envelope'></i></span>
						<input id='mdLogin-mail' type='email' class='form-control text-lowercase' name='login-email' value='' placeholder='e-mail fornecido no cadastro'>
					</div> -->
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="mail" id="mdLogin-mail" />
						<label class="input__label input__label--kaede" for="mdLogin-mail">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-envelope md-login-icon'></i><span>e-mail fornecido no cadastro</span></span>
						</label>
					</span>
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="password" id="mdLogin-password" />
						<label class="input__label input__label--kaede" for="mdLogin-password">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-lock md-login-icon'></i><span>senha de 6 a 10 caracteres</span></span>
						</label>
					</span>
					<!-- <div class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-lock'></i></span>
						<input id='mdLogin-password' type='password' class='form-control' name='password' placeholder='senha de 6 a 10 caracteres'>
					</div> -->
				</form>
				<div id='mdLogin-alert' class='alert alert-danger fade in' style='display:none;margin-top:20px;margin-bottom:0px'>
					<a href='#' class='close' aria-label='close'>&times;</a>
					<span>Preencha os campos</span>
				</div>
			</div>
			<div class='modal-footer'>
				<!-- <button id='mdLogin-btnLogar' type='submit' class='btn btn-default pull-left mdLogin-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-log-in'></span> Logar</button>
				<button id='mdLogin-btnCancelar' type='submit' class='btn btn-default pull-left mdLogin-btn' data-dismiss='modal' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-remove'></span> Cancelar</button> -->
				<button id='mdLogin-btnLogar' class="button button--tamaya" data-text="Logar"><span>Logar</span></button>
				<button id='mdLogin-btnCancelar' data-dismiss='modal' class="button button--tamaya" data-text="Cancelar"><span>Cancelar</span></button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Cadastrar -->
<div class='modal fade' id='mdCadastro' role='dialog' data-backdrop='static'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' style='padding:20px 40px;'>
				<h4>Formulário de cadastro na plataforma</h4>
			</div>
			<div class='modal-body' style='padding:25px'>
				<form role='form'>
					<p>O cadastro na plataforma é necessário apenas para motoristas de van</p>
					<!-- <div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-envelope'></i></span>
						<input id='mdCadastro-mail' type='text' class='form-control text-lowercase' name='mail' value='' placeholder='e-mail de contato'>
					</div> -->
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="mail" id="mdCadastro-mail" />
						<label class="input__label input__label--kaede" for="mdCadastro-mail">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-envelope md-login-icon'></i><span>e-mail de contato</span></span>
						</label>
					</span>
					<!-- <div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon glyphicon-earphone'></i></span>
						<input id='mdCadastro-fone' type='text' class='form-control text-lowercase' name='fone' value='' placeholder='entre com o seu fone'>
					</div> -->
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="mdCadastro-fone" />
						<label class="input__label input__label--kaede" for="mdCadastro-fone">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-earphone md-login-icon'></i><span>entre com o seu fone</span></span>
						</label>
					</span>
					<!-- <div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-lock'></i></span>
						<input id='mdCadastro-senha' type='password' class='form-control' name='password' placeholder='senha de 6 a 10 caracteres'>
					</div> -->
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="password" id="mdCadastro-senha" />
						<label class="input__label input__label--kaede" for="mdCadastro-senha">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-lock md-login-icon'></i><span>senha de 6 a 10 caracteres</span></span>
						</label>
					</span>
					<!-- <div class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-lock'></i></span>
						<input id='mdCadastro-confirma-senha' type='password' class='form-control' name='password' placeholder='confirmação da senha'>
					</div> -->
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="password" id="mdCadastro-confirma-senha" />
						<label class="input__label input__label--kaede" for="mdCadastro-confirma-senha">
							<span class="input__label-content input__label-content--kaede"><i class='glyphicon glyphicon-lock md-login-icon'></i><span>confirmação da senha</span></span>
						</label>
					</span>
				</form>
				<div id='mdCadastro-alert' class='alert alert-danger fade in' style='display:none;margin-top:20px;margin-bottom:0px'>
					<a href='#' class='close' aria-label='close'>&times;</a>
					<span>Preencha os campos</span>
				</div>
			</div>
			<div class='modal-footer'>
				<!-- <button id='mdCadastro-btnSubmeter' type='submit' class='btn btn-default pull-left mdCadastro-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-ok'></span> Submeter</button>
				<button id='mdCadastro-btnFechar' type='submit' class='btn btn-default pull-left mdCadastro-btn' data-dismiss='modal' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-remove'></span> Fechar</button> -->
				<button id='mdCadastro-btnSubmeter' type='submit' class="button button--tamaya" data-text="Enviar"><span>Enviar</span></button>
				<button id='mdCadastro-btnFechar' data-dismiss='modal' class="button button--tamaya" data-text="Fechar"><span>Fechar</span></button>

			</div>
		</div>
	</div>
</div>

<!-- Modal Editar Cadastro -->
<div class='modal fade' id='mdEditarCadastro' role='dialog' data-backdrop='static'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' style='padding:20px 40px;'>
				<h4>Formulário de cadastro na plataforma</h4>
			</div>
			<div class='modal-body' style='padding:25px'>
				<form role='form'>
					<p>O cadastro na plataforma é necessário apenas para motoristas de van</p>
					<div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-envelope'></i></span>
						<input id='mdEditarCadastro-mail' type='text' class='form-control text-lowercase' name='mail' value='' placeholder='e-mail de contato'>
					</div>
					<div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon glyphicon-earphone'></i></span>
						<input id='mdEditarCadastro-fone' type='text' class='form-control text-lowercase' name='fone' value='' placeholder='entre com o seu fone'>
					</div>
				</form>
				<div id='mdEditarCadastro-alert' class='alert alert-danger fade in' style='display:none;margin-top:20px;margin-bottom:0px'>
					<a href='#' class='close' aria-label='close'>&times;</a>
					<span>Preencha os campos</span>
				</div>
			</div>
			<div class='modal-footer'>
				<button id='mdEditarCadastro-btnSubmeter' type='submit' class='btn btn-default pull-left mdEditarCadastro-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-ok'></span> Submeter</button>
				<button id='mdEditarCadastro-btnFechar' type='submit' class='btn btn-default pull-left mdEditarCadastro-btn' data-dismiss='modal' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-remove'></span> Fechar</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal alterar senha -->
<div class='modal fade' id='mdEditarSenha' role='dialog' data-backdrop='static'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' style='padding:20px 40px;'>
				<h4>Formulário para alterar a sua senha</h4>
			</div>
			<div class='modal-body' style='padding:25px'>
				<form role='form'>
					<div style='margin-bottom: 10px' class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-lock'></i></span>
						<input id='mdEditarSenha-senha' type='password' class='form-control' name='password' placeholder='nova senha com 6 a 10 caracteres'>
					</div>
					<div class='input-group'>
						<span class='input-group-addon'><i class='glyphicon glyphicon-lock'></i></span>
						<input id='mdEditarSenha-confirma-senha' type='password' class='form-control' name='password' placeholder='confirmação da nova senha'>
					</div>
				</form>
				<div id='mdEditarSenha-alert' class='alert alert-danger fade in' style='display:none;margin-top:20px;margin-bottom:0px'>
					<a href='#' class='close' aria-label='close'>&times;</a>
					<span>Preencha os campos</span>
				</div>
			</div>
			<div class='modal-footer'>
				<button id='mdEditarSenha-btnSubmeter' type='submit' class='btn btn-default pull-left mdEditarSenha-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-ok'></span> Submeter</button>
				<button id='mdEditarSenha-btnFechar' type='submit' class='btn btn-default pull-left mdEditarSenha-btn' data-dismiss='modal' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-remove'></span> Fechar</button>
			</div>
		</div>
	</div>
</div>

<!-- cadastro de linha -->
<div class='modal fade' id='modalLinha' role='dialog'  data-backdrop='static'>
	<div class="modal-dialog">
		<div class='modal-content'>
			<div class="modal-header" style="padding: 20px 40px;">
				<h4>Escolha os pontos antes de cadastrar</h4>
			</div>
			<div class="modal-footer" style="padding:25px">
				<form role="form">
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="modeloVan" />
						<label class="input__label input__label--kaede" for="modeloVan">
							<span class="input__label-content input__label-content--kaede">Modelo da van</span>
						</label>
					</span>
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="horario" />
						<label class="input__label input__label--kaede" for="horario">
							<span class="input__label-content input__label-content--kaede">Horario de saida</span>
						</label>
					</span>
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="nrovagas" />
						<label class="input__label input__label--kaede" for="nrovagas">
							<span class="input__label-content input__label-content--kaede">Numero de vagas</span>
						</label>
					</span>
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="ano" />
						<label class="input__label input__label--kaede" for="ano">
							<span class="input__label-content input__label-content--kaede">Ano do modelo</span>
						</label>
					</span>
					<span class="input input--kaede">
						<input class="input__field input__field--kaede" type="text" id="nome" />
						<label class="input__label input__label--kaede" for="nome">
							<span class="input__label-content input__label-content--kaede">Nome da linha</span>
						</label>
					</span>
				</form>
			</div>
			<div class="modal-footer">
				<!-- <input type="submit" data-dismiss='modal' style='background-color: white;' id="submit-cadastro-linha" name="enviar" value="enviar"> -->
				<button id='submit-cadastro-linha' class="button button--tamaya" data-text="Enviar"><span>Enviar</span></button>
				<!-- <input type="submit" data-dismiss='modal' style='background-color: white;' id="cancelar-cadastro-linha" name="cancelar" value="cancelar"> -->
				<button id='cancelar-cadastro-linha' data-dismiss='modal' class="button button--tamaya" data-text="Cancelar"><span>Cancelar</span></button>
			</div>
		</div>
	</div>
</div>

<!-- Modal login -->
<div class='modal fade' id='modalLeitura' role='dialog' data-backdrop='static'>
	<div class='modal-dialog'>
		<!-- Modal content-->
		<div class='modal-content'>
			<div class='modal-header' style='padding:20px 40px;'>
				<h4 id='modalLeituraTitulo'>Coleta no dia</h4>
			</div>
			<div class='modal-body' style='padding:25px'>
				<form role='form' id='form-coleta' >
					<input type='hidden' value='' id='leitura-data'>
					<div style='margin-bottom: 15px' class='input-group'>
						<span class='input-group-addon'>Digite a recipitação no dia</span>
						<input id='leitura-valor' type='text' class='form-control' name='leitura-valor' value=''>
					</div>
					<p>Forneça o valor 0 no caso de não ter chovido e clique no botão <code>Sem coleta</code> no caso de não ter sido possível registrar a precipitação nesta data.</p>
				</form>
				<div id='modalLeitura-alert' class='alert alert-danger fade in' style='display:none;margin-top:20px;margin-bottom:0px'>
					<a href='#' class='close' aria-label='close'>&times;</a>
					<span>Preencha os campos</span>
				</div>
			</div>
			<div class='modal-footer'>
				<button id='modalLeitura-btnRegistrar' type='submit' class='btn btn-default pull-left modalLeitura-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-send'></span> Registrar</button>
				<button id='modalLeitura-btnSemColeta' type='submit' class='btn btn-default pull-left modalLeitura-btn' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-send'></span> Sem coleta</button>
				<button id='modalLeitura-btnCancelar' type='submit' class='btn btn-default pull-left modalLeitura-btn' data-dismiss='modal' data-loading-text="<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processando..."><span class='glyphicon glyphicon-remove'></span> Cancelar</button>
			</div>
		</div>
	</div>
</div>
